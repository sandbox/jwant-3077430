<?php

namespace Drupal\facet_groups\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Facet group entities.
 */
interface FacetGroupInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
