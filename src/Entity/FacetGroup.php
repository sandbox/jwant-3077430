<?php

namespace Drupal\facet_groups\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Facet group entity.
 *
 * @ConfigEntityType(
 *   id = "facet_group",
 *   label = @Translation("Facet group"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\facet_groups\FacetGroupListBuilder",
 *     "form" = {
 *       "add" = "Drupal\facet_groups\Form\FacetGroupForm",
 *       "edit" = "Drupal\facet_groups\Form\FacetGroupForm",
 *       "delete" = "Drupal\facet_groups\Form\FacetGroupDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\facet_groups\FacetGroupHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "facet_group",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/config/search/facets/groups/{facet_group}",
 *     "add-form" = "/admin/config/search/facets/groups/add",
 *     "edit-form" = "/admin/config/search/facets/groups/{facet_group}/edit",
 *     "delete-form" = "/admin/config/search/facets/groups/{facet_group}/delete",
 *     "collection" = "/admin/config/search/facets/groups"
 *   }
 * )
 */
class FacetGroup extends ConfigEntityBase implements FacetGroupInterface {

  /**
   * The Facet group ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Facet group label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Facets by reference.
   *
   * @var array
   */
  public $facets = [];


}
