<?php

namespace Drupal\facet_groups\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class FacetGroupForm.
 */
class FacetGroupForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $facet_group = $this->entity;

    // Transform stored facets into structure for the form.
    if ($facet_group) {

    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $facet_group->label(),
      '#description' => $this->t("Label for the Facet group."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $facet_group->id(),
      '#machine_name' => [
        'exists' => '\Drupal\facet_groups\Entity\FacetGroup::load',
      ],
      '#disabled' => !$facet_group->isNew(),
    ];

    // Gather the number of facets already selected. If not set, default to 1.
    $num_facets = $form_state->get('num_facets');
    if ($num_facets === NULL) {
      $num_facets = 1;
    }

    $form['#tree'] = TRUE;
    $form['facets_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Grouped Facets'),
      '#prefix' => '<div id="facets-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    for ($i = 0; $i < $num_facets; $i++) {
      $facets = $form_state->getValue('facets_fieldset')['facets'];
      $default = isset($facets[$i]) ? $facets[$i] : '';
      $form['facets_fieldset']['facets'][$i] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'facets_facet',
        '#title' => $this->t('Facet'),
        '#description' => $this->t('Select a facet to group.'),
        '#required' => TRUE,
        '#default_value' => $default,

      ];
    }

    $form['facets_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['facets_fieldset']['actions']['add_facet'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another facet'),
//      '#limit_validation_errors' => [$form['facets_fieldset']],
      '#limit_validation_errors' => [],
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'facets-fieldset-wrapper',
      ],
    ];
    // If there is more than one facet, add the remove button.
    if ($num_facets > 1) {
      $form['facets_fieldset']['actions']['remove_facet'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove facet'),
        '#limit_validation_errors' => [],
//        '#limit_validation_errors' => [$form['facets_fieldset']],
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'facets-fieldset-wrapper',
        ],
      ];
    }

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['facets_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $facet_field = $form_state->get('num_facets');
    $add_button = $facet_field + 1;
    $form_state->set('num_facets', $add_button);

    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $facet_field = $form_state->get('num_facets');
    if ($facet_field > 1) {
      $remove_button = $facet_field - 1;
      $form_state->set('num_facets', $remove_button);
    }

    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Shuffle selected Facets into a single form state item, keyed to match
    // the Facets entity property.
    $facets = $form_state->getValue('facets_fieldset')['facets'];
    $form_state->setValue('facets', $facets);
    parent::submitForm($form, $form_state);
  }



  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $facet_group = $this->entity;
    $status = $facet_group->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Facet group.', [
          '%label' => $facet_group->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Facet group.', [
          '%label' => $facet_group->label(),
        ]));
    }
    $form_state->setRedirectUrl($facet_group->toUrl('collection'));
  }

}
