<?php
/**
 * @file
 * Contains \Drupal\facet_groups\Element\FacetGroupFacet.
 */

namespace Drupal\facet_groups\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a compound form element for Facet reference and Label.
 *
 * @FormElement("facet_group_facet")
 */
class FacetGroupFacet extends RenderElement {
  /**
   * {@inheritdoc}
   */
  public function getInfo() {

    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#theme' => 'facet_group_facet',
      '#label' => 'Group Facet',
      '#description' => 'Select a facet and customize its label.',
      '#process' => [$class, 'processGroupFacet'],
      '#pre_render' => [$class, 'preRenderGroupFacet'],
    ];

  }

  /**
   * Prepare the render array for the template.
   */
  public static function preRenderGroupFacet($element) {

    // Facet selection for available facets.
//    $manager = \Drupal::entityTypeManager()->getStorage('facets_facet');
//    $facets = $manager->getQuery()->exists('widget')->execute();

    $element['facet_id'] = [
      '#type' => 'entity_autocomplete',
      '#attributes' => ['name' => $element['#name'] . '_facet_id'],
      '#required' => $element['#required'],
      '#target_type' => 'facets_facet',
    ];

    // Last name
    $element['facet_label'] = [
      '#type' => 'textfield',
      '#attributes' => ['name' => $element['#name'] . '_group_facet_label'],
      '#required' => $element['#required'],
    ];

    // Default value.  Note that format is an associative array.
    $element['#default_value'] = ['facet_id', 'facet_label'];

    return $element;
  }

  /**
   * Copy the user inputs to the parent field value.
   */
  public static function processGroupFacet(&$element, FormStateInterface $form_state, &$complete_form) {

    // Get all form inputs
    // @Todo: Do not use user input directly.
    $inputs = $form_state->getUserInput();

    // Get the parent field name
    $name = $element['#name'];

    // If the user entered a first and last name, then prep it for storage in form_state
    if ( isset($inputs["{$name}_facet_id"]) && isset($inputs["{$name}_group_facet_label"]) ) {
      $element['#value'] = ['facet_id' =>  $inputs["{$name}_facet_id"], 'facet_label' =>  $inputs["{$name}_group_facet_label"] ];
      $form_state->setValue($name, $element['#value']);
    }

    return $element;
  }

}